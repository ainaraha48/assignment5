function validateForm()
{
	var name = document.forms["info"]["name"].value;
	var length = document.forms["info"]["length"].value;
	var width = document.forms["info"]["width"].value;
	var height = document.forms["info"]["height"].value;
	var mode = document.forms["info"]["mode"].value;
	var type = document.forms["info"]["type"].value;
	var weight;
	var cost;

	weight = (length*width*height)/5000;

	alert("Parcel Volumetric and Cost Calculator"+"\nCustomer name: "+upperCase(name)+"\nLength: "+length+"cm"+"\nWidth: "+width+"cm"+"\nHeight: "+height+"cm"+"\nWeight: "+weight+"kg"+"\nMode: "+mode+"\nType: "+type+"\nDelivery cost: RM"+calculateCostParcel(type,weight,mode));
}

function warning()
{
	alert("The input will be reset");
}

function upperCase(name)
{
	str = name.toUpperCase();
	return str;
}

function calculateCostParcel(type,weight,mode)
{
	var cost;
	if(type=="Domestic")
	{
		if(weight<2.00)
		{
			if(mode=="Surface")
			{
				cost=7;
			}
			else if(mode=="Air")
			{
				cost=10;
			}
		}
		else
		{
			if(mode=="Surface")
			{
				cost=7+((weight-2.00)*1.5);
			}
			else if(mode=="Air")
			{
				cost=10+((weight-2.00)*3);
			}
		}
	}
	else if(type=="International")
	{
		if(weight<2.00)
		{
			if(mode=="Surface")
			{
				cost=20;
			}
			else if(mode=="Air")
			{
				cost=50;
			}
		}
		else
		{
			if(mode=="Surface")
			{
				cost=20+((weight-2.00)*3);
			}
			else if(mode=="Air")
			{
				cost=50+((weight-2.00)*5);
			}
		}
	}
	return cost;
}